import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className='container'>
        <Routes>
          <Route index element={<MainPage />} />
          <Route path='conferences'>
            <Route path='new' element={<ConferenceForm />}></Route>
          </Route>
          <Route path='attendees'>
            <Route index element={<AttendeesList attendees={props.attendees} />}></Route>
            <Route path='new' element={<AttendConferenceForm />}></Route>
          </Route>
          <Route path='locations'>
            <Route path='new' element={<LocationForm />}></Route>
          </Route>
          <Route path='presentations'>
            <Route path='new' element={<PresentationForm />}></Route>
          </Route>
          <Route path='*' element={<h1>404 Page Not Found</h1>}></Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
